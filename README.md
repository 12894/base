#include <iostream>
#include <vector>

void Bases(const int &c, const int& base)
{
	std::vector<int> guardar;
	int x;
	int y;
	x = c / base;
	y = c % base;
	guardar.push_back(y);
	while (x > 0)
	{
		y = x % base;
		x /= base;
		guardar.push_back(y);
	}

	for (int i = guardar.size(); i--;)
	{
	    if (guardar.at(i) == 10) {
			std::cout << "A";
		}
		else	if (guardar.at(i) == 11) {
			std::cout << "B";
		}
		else	if (guardar.at(i) == 12) {
			std::cout << "C";
		}
		else	if (guardar.at(i) == 13) {
			std::cout << "D";
		}
		else	if (guardar.at(i) == 14) {
			std::cout << "E";
		}
		else	if (guardar.at(i) == 15) {
			std::cout << "F";
		}
		else	if (guardar.at(i) == 16) {
			std::cout << "g";
		}
		else	if (guardar.at(i) == 17) {
			std::cout << "h";
		}
		else	if (guardar.at(i) == 18) {
			std::cout << "i";
		}
		else	if (guardar.at(i) == 19) {
			std::cout << "j";
		}
		else	if (guardar.at(i) == 20) {
			std::cout << "k";
		}
		else	if (guardar.at(i) == 21) {
			std::cout << "l";
		}
		else	if (guardar.at(i) == 22) {
			std::cout << "m";
		}
		else	if (guardar.at(i) == 23) {
			std::cout << "n";
		}
		else	if (guardar.at(i) == 24) {
			std::cout << "o";
		}
		else	if (guardar.at(i) == 25) {
			std::cout << "p";
		}
		else	if (guardar.at(i) == 26) {
			std::cout << "q";
		}
		else	if (guardar.at(i) == 27) {
			std::cout << "r";
		}
		else	if (guardar.at(i) == 28) {
			std::cout << "s";
		}
		else	if (guardar.at(i) == 29) {
			std::cout << "t";
		}
		else	if (guardar.at(i) == 30) {
			std::cout << "u";
		}
		else	if (guardar.at(i) == 31) {
			std::cout << "v";
		}
		else	if (guardar.at(i) == 32) {
			std::cout << "w";
		}
		else	if (guardar.at(i) == 33) {
			std::cout << "x";
		}
		else	if (guardar.at(i) == 34) {
			std::cout << "y";
		}
		else	if (guardar.at(i) == 35) {
			std::cout << "z";
		}
		else if (guardar.at(i) > 9)
		{
			guardar.at(i) = guardar.at(i) + 55;
			std::cout << (int)guardar.at(i);
		}
		else std::cout << guardar.at(i);
	}

}

void main()
{
	int numero, base;
	std::cout << "numero" << std::endl;
	std::cin >> numero;
	std::cout << "base" << std ::endl;
	std::cin >> base;
	std::cout << "Tu resultado es:" << "\n";
	Bases(numero, base);
	
}
